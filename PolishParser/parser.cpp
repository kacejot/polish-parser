#include "parser.h"

namespace Calculator 
{
	double ctg(double value)
	{
		return 1 / std::tan(value);
	}

	term::term() {
		term_type = type::NoType;
		term_value = "";
		term_priority = priority::NoPriority;
	}

	term::term(std::string value, type type) {
		term_type = type;
		term_value = value;
		get_priority();
	}

	void term::get_priority()
	{
		switch (term_type)
		{
		case Function:
			term_priority = priority::Max;
			break;
		case Operator:
		{
			if (term_value == "+" || term_value == "-")
			{
				term_priority = priority::Low;
				break;
			}
			if (term_value == "*" || term_value == "/")
			{
				term_priority = priority::Medium;
				break;
			}
			if (term_value == "^")
			{
				term_priority = priority::High;
				break;
			}
			if (term_value == "(" || term_value == ")")
			{
				term_priority = priority::NoPriority;
				break;
			}
		}
		case Unary:
			term_priority = priority::Low;
			break;
		default:
			term_priority = priority::NoPriority;
			break;
		}
	}

	parser::parser()
	{
		functors_map =
		{
			{ "exp", std::exp },
			{ "sin", std::sin },
			{ "cos", std::cos },
			{ "tan", std::tan },
			{ "ctg", ctg }
		};

		constants_map = 
		{
			{ "Pi", 3.141592653589793 },
			{ "E", 2.718281828459045 }
		};
	}

	void parser::set_expression(std::string expr)
	{
		variables_map.clear();
		input_expression = expr;
		from_infix();
		perform(true);
		unary_is_posible = true;
	}

	bool parser::set_variable_value(std::string name, double value)
	{
		auto it = variables_map.begin();
		if ((it = variables_map.find(name)) != variables_map.end())
		{
			it->second = value;
			return true;
		}
		return false;
	}

	bool parser::add_function(std::string name, functor function)
	{
		if (functors_map.find(name) == functors_map.end() &&
			variables_map.find(name) == variables_map.end() &&
			constants_map.find(name) == constants_map.end())
		{
			functors_map.emplace(name, function);
			return true;
		}
		else
		{
			return false;
		}
	}

	double parser::calculate()
	{
		return perform(false);
	}

	void parser::from_infix()
	{
		std::string current_number = "";
		std::string current_word = "";
		for (size_t i = 0; i < input_expression.size(); ++i)
		{
			char current_char = input_expression[i];
			// Skip spaces
			if (std::isspace(current_char))  
			{
				continue;
			}
			// Add number to prefix expression 
			if (std::isdigit(current_char) || current_char == '.')
			{
				found_digit(i, current_number);
				continue;
			}
			// Push function to a stack or variables to output
			if (std::isalpha(current_char)) 
			{
				found_alpha(i, current_word);
				continue;
			}
			// Add open bracket tp a stack
			if (current_char == '(')
			{
				found_open_bracket();
				continue;
			}
			// While top object is not open bracket - pop stack to an output
			if (current_char == ')')
			{
				found_close_bracket();
				continue;
			}
			// Push operators to stack
			found_arithmetical_operator(current_char);
		}
		while (!operators_stack.empty())
		{
			prefix_expression.push_back(operators_stack.back());
			operators_stack.pop_back();
		}
	}

	double parser::perform(bool optimize = false)
	{
		std::vector<term> optimized_expression;
		operators_stack.clear();
		for (auto element : prefix_expression) 
		{
			switch (element.term_type)
			{
			case Value:
			case Variable:
				operators_stack.push_back(element);
				break;
			case Operator:
				perform_operator(element, optimize);
				break;
			case Unary:
				perform_unary_operator(element, optimize);
				break;
			case Function:
				perform_function(element, optimize);
				break;
			default:
				throw std::exception("Unknown term type");
				break;
			}
		}
		if (optimize)
		{
			optimized_expression = std::vector<term>(operators_stack.begin(), operators_stack.end());
			prefix_expression = optimized_expression;
		}
		else if (operators_stack.size() == 1)
		{
			return std::stod(operators_stack.back().term_value);
		}
		return 0;
	}

	void parser::found_digit(size_t i, std::string& current_number)
	{
		char c = input_expression[i];
		size_t input_size = input_expression.length();
		current_number.push_back(c);
		if (i + 1 < input_size && !isdigit(input_expression[i + 1]) || i == input_size - 1)
		{
			prefix_expression.emplace_back(current_number, type::Value);
			current_number.clear();
		}
		unary_is_posible = false;
	}

	void parser::found_alpha(size_t i, std::string& current_word)
	{
		char c = input_expression[i];
		size_t input_size = input_expression.length();
		current_word.push_back(c);
		if (i + 1 < input_size && !isalpha(input_expression[i + 1]) || i == input_size - 1)
		{
			if (functors_map.find(current_word) != functors_map.end())
			{
				operators_stack.emplace_back(current_word, type::Function);
			}
			else if (constants_map.find(current_word) != constants_map.end())
			{
				prefix_expression.emplace_back(std::to_string(constants_map[current_word]), type::Value);
			}
			else
			{
				variables_map[current_word] = 0;
				prefix_expression.emplace_back(current_word, type::Variable);
			}
			current_word.clear();
		}
		unary_is_posible = false;
	}

	void parser::found_open_bracket()
	{
		operators_stack.emplace_back("(", type::Operator);
		unary_is_posible = true;
	}

	void parser::found_close_bracket()
	{
		while (true)
		{
			if (operators_stack.back().term_value == "(")
			{
				operators_stack.pop_back();
				break;
			}
			else
			{
				prefix_expression.push_back(operators_stack.back());
				operators_stack.pop_back();
			}
		}
		unary_is_posible = false;
	}

	void parser::found_arithmetical_operator(char c)
	{
		{
			term t;
			switch (c)
			{
			case '+':
				t = term("+", type::Operator);
				break;
			case '-':
				if (unary_is_posible)
				{
					t = term("-", type::Unary);
				}
				else
				{
					t = term("-", type::Operator);
				}
				break;
			case '/':
				t = term("/", type::Operator);
				break;
			case '*':
				t = term("*", type::Operator);
				break;
			case '^':
				t = term("^", type::Operator);
				break;
			default:
				throw std::exception("Unknown operator symbol");
				break;
			}
			term temp;
			if (!operators_stack.empty()) {
				temp = operators_stack.back();
			}
			while (t.term_priority <= temp.term_priority &&
				temp.term_priority != priority::NoPriority)
			{
				prefix_expression.push_back(operators_stack.back());
				operators_stack.pop_back();

				if (!operators_stack.empty()) {
					temp = operators_stack.back();
				}
				else
				{
					break;
				}
			}
			operators_stack.push_back(t);
		}
		unary_is_posible = false;
	}

	void parser::perform_operator(term term_operator, bool optimize)
	{
		if (operators_stack.size() < 2)
		{
			throw std::exception("There is no operands to do this operation.");
			return;
		}
		term right = operators_stack.back();
		term left = *(operators_stack.end() - 2);
		if (!optimize ||
			((left.term_type == type::Value) &&
			(right.term_type == type::Value)))
		{
			double left_value = 0;
			double right_value = 0;
			operators_stack.pop_back();
			operators_stack.pop_back();

			if (!optimize && left.term_type == type::Variable)
			{
				left_value = variables_map[left.term_value];
			}
			else
			{
				left_value = std::stod(left.term_value);
			}
			if (!optimize && right.term_type == type::Variable)
			{
				right_value = variables_map[right.term_value];
			}
			else
			{
				right_value = std::stod(right.term_value);
			}

			double result = 0;

			if (term_operator.term_value == "+")
			{
				result = left_value + right_value;
			}
			else if (term_operator.term_value == "-")
			{
				result = left_value - right_value;
			}
			else if (term_operator.term_value == "*")
			{
				result = left_value * right_value;
			}
			else if (term_operator.term_value == "/")
			{
				result = left_value / right_value;
			}
			else if (term_operator.term_value == "^")
			{
				result = std::pow(left_value, right_value);
			}
			else
			{
				throw std::exception("Unknown operator.");
			}
			operators_stack.emplace_back(std::to_string(result), type::Value);
		}
		else
		{
			operators_stack.push_back(term_operator);
		}
	}

	void parser::perform_unary_operator(term term_operator, bool optimize)
	{
		if (operators_stack.size() < 1)
		{
			throw std::exception("There is no operands to do this operation.");
			return;
		}
		term string_value = operators_stack.back();
		if (!optimize || string_value.term_type == type::Value)
		{
			double value = 0;
			operators_stack.pop_back();
			if (!optimize && string_value.term_type == type::Variable)
			{
				value = variables_map[string_value.term_value];
			}
			else
			{
				value = std::stod(string_value.term_value);
			}
			double result = 0;
			if (term_operator.term_value == "-")
			{
				result = -value;
			}
			else
			{
				throw std::exception("Unknown operator.");
			}
			operators_stack.emplace_back(std::to_string(result), type::Value);
		}
		else
		{
			operators_stack.push_back(term_operator);
		}
	}

	void parser::perform_function(term term_function, bool optimize)
	{
		if (operators_stack.size() < 1)
		{
			throw std::exception("There is no operands to do this operation.");
			return;
		}
		term string_value = operators_stack.back();
		if (!optimize || string_value.term_type == type::Value)
		{
			double value = 0;
			operators_stack.pop_back();
			if (!optimize && string_value.term_type == type::Variable)
			{
				value = variables_map[string_value.term_value];
			}
			else
			{
				value = std::stod(string_value.term_value);
			}
			double result = 0;
			auto ptr = functors_map.begin();
			if ((ptr = functors_map.find(term_function.term_value)) != functors_map.end())
			{
				result = ptr->second(value);
			}
			else
			{
				throw std::exception("Unknown function.");
			}
			operators_stack.emplace_back(std::to_string(result), type::Value);
		}
		else
		{
			operators_stack.push_back(term_function);
		}
	}
}



	
