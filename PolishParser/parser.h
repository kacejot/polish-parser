#pragma once
#include <stack>
#include <string>
#include <vector>
#include <map>
#include <cctype>
#include <cmath>
#include <iostream>

namespace Calculator
{
	enum type { Function, Value, Variable, Operator, Unary, NoType };
	enum priority { NoPriority, Low, Medium, High, Max };

	double ctg(double value);

	struct term
	{
	public:
		std::string		 term_value;
		priority		 term_priority;
		type			 term_type;

		term();

		term(std::string value, type type);

	private:
		void get_priority();
	};


	class parser 
	{
	public:
		typedef double(*functor)(double);

		parser();

		void set_expression(std::string expr);

		bool set_variable_value(std::string name, double value);

		bool add_function(std::string name, functor function);

		double calculate();

	private:
		bool							 unary_is_posible;
		std::string						 input_expression;
		std::vector<term>				 operators_stack;
		std::vector<term>				 prefix_expression;
		std::map<std::string, functor>	 functors_map;
		std::map<std::string, double>	 variables_map;
		std::map<std::string, double>	 constants_map;

		void from_infix();

		double perform(bool);

		void found_digit(size_t, std::string&);

		void found_alpha(size_t, std::string&);

		void found_open_bracket();

		void found_close_bracket();

		void found_arithmetical_operator(char);

		void perform_operator(term, bool);

		void perform_unary_operator(term, bool);

		void perform_function(term, bool);
	};

}
